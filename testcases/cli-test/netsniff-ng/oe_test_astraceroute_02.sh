#!/usr/bin/bash
# Copyright (c) 2023. huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
# #############################################
# @Author    :   zhuwenshuo
# @Contact   :   1003254035@qq.com
# @Date      :   2023/02/21
# @License   :   Mulan PSL v2
# @Desc      :   Test astraceroute
# #############################################
source "$OET_PATH/libs/locallibs/common_lib.sh"
function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL "netsniff-ng tar"
    tar -xvf ./common/data.tar.gz > /dev/null
    LOG_INFO "End to prepare the test environment."
}
function run_test() {
    LOG_INFO "Start to run test."
    astraceroute -i ${NODE1_NIC} -H netsniff-ng.org -m 50 -f 49 > tmp.txt &
    SLEEP_WAIT 5
    grep "49:" tmp.txt
    CHECK_RESULT $? 0 0 "Check astraceroute -m failed"
    kill -9 $(pgrep -f "astraceroute -i")
    rm -f tmp.txt
    # test --max-ttl
    astraceroute -i ${NODE1_NIC} -H netsniff-ng.org --max-ttl 50 -f 49 > tmp.txt &
    SLEEP_WAIT 5
    grep "49:" tmp.txt
    CHECK_RESULT $? 0 0 "Check astraceroute --max-ttl failed"
    kill -9 $(pgrep -f "astraceroute -i")
    rm -f tmp.txt
    # test -q
    astraceroute -i ${NODE1_NIC} -H netsniff-ng.org -q 1 > tmp.txt &
    SLEEP_WAIT 5
    grep "AS path IPv4 TCP trace from" tmp.txt
    CHECK_RESULT $? 0 0 "Check astraceroute -q failed"
    kill -9 $(pgrep -f "astraceroute -i")
    rm -f tmp.txt
    # test --num-probes
    astraceroute -i ${NODE1_NIC} -H netsniff-ng.org --num-probes 1 > tmp.txt &
    SLEEP_WAIT 5
    grep "AS path IPv4 TCP trace from" tmp.txt
    CHECK_RESULT $? 0 0 "Check astraceroute --num-probes failed"
    kill -9 $(pgrep -f "astraceroute -i")
    rm -f tmp.txt
    # test -x
    astraceroute -i ${NODE1_NIC} -H netsniff-ng.org -x 2 > tmp.txt &
    SLEEP_WAIT 5
    grep "AS path IPv4 TCP trace from" tmp.txt
    CHECK_RESULT $? 0 0 "Check astraceroute -x failed"
    kill -9 $(pgrep -f "astraceroute -i")
    rm -f tmp.txt
    astraceroute -i ${NODE1_NIC} -H netsniff-ng.org --timeout 1 > tmp.txt &
    SLEEP_WAIT 5
    grep "AS path IPv4 TCP trace from" tmp.txt
    CHECK_RESULT $? 0 0 "Check astraceroute --timeout failed"
    kill -9 $(pgrep -f "astraceroute -i")
    rm -f tmp.txt
    # test -X
    astraceroute -i ${NODE1_NIC} -H netsniff-ng.org -X "censor me" > tmp.txt &
    SLEEP_WAIT 5
    grep "censor me" tmp.txt
    CHECK_RESULT $? 0 0 "Check astraceroute -X failed"
    kill -9 $(pgrep -f "astraceroute -i")
    rm -f tmp.txt
    # test --payload
    astraceroute -i ${NODE1_NIC} -H netsniff-ng.org --payload "censor me" > tmp.txt &
    SLEEP_WAIT 5
    grep "censor me" tmp.txt
    CHECK_RESULT $? 0 0 "Check astraceroute --payload failed"
    kill -9 $(pgrep -f "astraceroute -i")
    rm -f tmp.txt
    # test -l
    astraceroute -i ${NODE1_NIC} -H netsniff-ng.org -X "sdakh" -Z -p 1 -l 20 > tmp.txt &
    SLEEP_WAIT 5
    grep "sdakh" tmp.txt
    CHECK_RESULT $? 0 0 "Check astraceroute -l failed"
    kill -9 $(pgrep -f "astraceroute -i")
    rm -f tmp.txt
    # test --totlen
    astraceroute -i ${NODE1_NIC} -H netsniff-ng.org -X "sdakh" -Z -p 1 --totlen 20 > tmp.txt &
    SLEEP_WAIT 5
    grep "sdakh" tmp.txt
    CHECK_RESULT $? 0 0 "Check astraceroute --totlen failed"
    kill -9 $(pgrep -f "astraceroute -i")
    rm -f tmp.txt
    # test -4
    astraceroute -i ${NODE1_NIC} -H netsniff-ng.org -4 > tmp.txt &
    SLEEP_WAIT 5
    grep "AS path IPv4 TCP trace from" tmp.txt
    CHECK_RESULT $? 0 0 "Check astraceroute -4 failed"
    kill -9 $(pgrep -f "astraceroute -i")
    rm -f tmp.txt
    # test --ipv4
    astraceroute -i ${NODE1_NIC} -H netsniff-ng.org --ipv4 > tmp.txt &
    SLEEP_WAIT 5
    grep "AS path IPv4 TCP trace from" tmp.txt
    CHECK_RESULT $? 0 0 "Check astraceroute --ipv4 failed"
    kill -9 $(pgrep -f "astraceroute -i")
    rm -f tmp.txt
    LOG_INFO "End to run test."
}
function post_test() {
    LOG_INFO "Start to restore the test environment."
    DNF_REMOVE
    rm -rf ./data/ ./common/*.sh
    LOG_INFO "End to restore the test environment."
}
main "$@"
