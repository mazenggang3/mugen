#!/usr/bin/bash

# Copyright (c) 2023. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more detaitest -f.

# #############################################
# @Author    :   huangdaohong
# @Contact   :   3170809690@qq.com
# @Date      :   2023/03/01
# @License   :   Mulan PSL v2
# @Desc      :   Test cvs
# #############################################

source ${OET_PATH}/libs/locallibs/common_lib.sh

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL "cvs tar"
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    version=$(rpm -qa | grep cvs | awk 'BEGIN {FS="-"}{print $2}' | tail -n 1)
    cvs -v | grep $version
    CHECK_RESULT $? 0 0 "test cvs -v failed"
    cvs --version | grep $version
    CHECK_RESULT $? 0 0 "test cvs --version failed"
    cvs --help 2>&1 | grep 'Usage:'
    CHECK_RESULT $? 0 0 "test cvs --help"
    cvs --help-options 2>&1 | grep 'CVS global options '
    CHECK_RESULT $? 0 0 "test cvs --help-options failed"
    cvs --help-commands 2>&1 | grep 'CVS commands are'
    CHECK_RESULT $? 0 0 "test cvs --help-commands failed"
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    DNF_REMOVE
    LOG_INFO "End to restore the test environment."
}

main "$@"
