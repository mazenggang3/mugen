= HELLO WORLD(1)
yangshicheng
v1.0.0
:doctype: manpage
:manmanual: HELLO WORLD
:mansource: HELLO WORLD
:man-linkstyle: pass:[blue R < >]

== Name

HELLO WORLD - this is a simple test for asciidoc

== Synopsis

*test* [_OPTION_]... _FILE_...

== Options

*-o, --out-file*=_OUT_FILE_::
Write result to file _OUT_FILE_.

*-c, --capture*::
Capture specimen if it's a picture of a life form.

== Exit status

<h1>This is heading</h1>

*1*::
Success.
Image is a picture of a life form.

*2*::
Failure.
Image is not a picture of a life form.

== Resources

*Project web site:* https://ysc.example.org

== Copying

Copyright 2008 {author}. +

