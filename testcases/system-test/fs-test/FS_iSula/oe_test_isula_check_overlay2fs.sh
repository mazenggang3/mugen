#!/usr/bin/bash

# Copyright (c) 2022. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
#@Author    	:   @meitingli
#@Contact   	:   bubble_mt@outlook.com
#@Date      	:   2021-06-17
#@License   	:   Mulan PSL v2
#@Desc      	:   check isula overlay2fs
#####################################

source ${OET_PATH}/libs/locallibs/common_lib.sh

function pre_test() {
    LOG_INFO "Start to prepare the database config."
    DNF_INSTALL iSulad
    if [[ ${NODE1_FRAME} == "aarch64" ]]; then
        wget https://repo.openeuler.org/openEuler-20.03-LTS-SP1/docker_img/aarch64/openEuler-docker.aarch64.tar.xz
        image_name="openEuler-docker.aarch64.tar.xz"
    else 
        wget https://repo.openeuler.org/openEuler-20.03-LTS-SP1/docker_img/x86_64/openEuler-docker.x86_64.tar.xz
        image_name="openEuler-docker.x86_64.tar.xz"
    fi
    systemctl start isulad
    isula load -i $image_name
    image_id=$(isula images | grep openeuler-20.03-lts-sp1 | head -n 1 | awk '{print $3}')
    isula create $image_id
    mkdir -p /home/isula_dir
    isula_id=$(isula ps -a | grep "Created" | head -n 1 | awk '{print $1}')
    isula start $isula_id
    LOG_INFO "Finish to prepare the database config."
}

function run_test() {
    LOG_INFO "Start to run test."
    isula inspect $isula_id | grep cgroup.dir | grep "/isulad"
    CHECK_RESULT $? 0 0 "The isula doesn't have /home/isula_dir."
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    isula stop -f $isula_id
    isula rm $isula_id
    isula rmi $(docker images -q)
    DNF_REMOVE
    rm -rf /home/isula_dir $image_name
    systemctl stop isulad
    LOG_INFO "End to restore the test environment."
}

main "$@"

